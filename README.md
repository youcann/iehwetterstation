Ieh-Wetterstation Software
==========================

##TODO
* php functions: $temp, $hmuid, ... getLive(), getValuesRaw(), getValuesAveraged(days, hours, minutes) 
* html/css/php seite für live ansicht mit "festem" layout, abhängig von $_GET[] + KIT rundEckig design rahmen

* python liveDaten: logger, fehlerbehandlung überdenken (vll + e-mail)
* python kontinuierlicher mittelwertbildner
* python diskonti...

* python sachen alle mit APscheduler
* alle python sachen mit system.d als service starten

* (raspian watchdog)
* (cal barometer)


###TODO Marvin
* Doku
* KIT Rahmen
* MQTT dokumentieren

###TODO Olena
* RFID: Regel: Bei Anwesenheit Steckdosen schalten
* RFID-Python-Skript ebenfalls mit system.d starten/laufen lassen

* Doku zu smarthome sachen
* owncloud
* präsenzerkennung(wifi, bluetooth)
